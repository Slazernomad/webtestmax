﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTesetRepository.DaoModels;

namespace WebTesetRepository.API
{
    public interface IGoodRepository
    {
        
        
            GoodDao[] GetAllGoods();
            GoodDao GetGoodById(int? id);
            bool AddNewGood(GoodDao organization);
            bool UpdateGood(GoodDao organization);
            bool RemoveGoodById(int id);


          

        }
    }

