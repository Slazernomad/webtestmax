﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTesetRepository.DaoModels;

namespace WebTesetRepository.API
{
    public interface ITagRepository
    {
        IEnumerable<TagDao> GetTagByKey(string key);
        bool AddNewTag(string tag);
    }
}
