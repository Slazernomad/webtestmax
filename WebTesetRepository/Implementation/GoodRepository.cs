﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using WebTesetRepository.API;
using WebTesetRepository.DaoModels;

namespace WebTesetRepository.Implementation
{
   public sealed class GoodRepository: IGoodRepository
    {
       
            private static List<GoodDao> _goods;
            private static int _idCounter;
            private static readonly object ThisLock = new object();

            static GoodRepository()
            {
                Random rand = new Random();
                _goods = new List<GoodDao>();
                for (int i = 0; i < 100; i++)
                {
                    _goods.Add(new GoodDao
                    {
                        Id = ++_idCounter,
                        Annotation = "Annotation #" + _idCounter,
                        Text = "Text here will be",
                        Inner = "inner text" + _idCounter,
                        CreationDate = DateTime.Now,
                        EditDate = null,
                        TagsStr = "1,2,3,4,5,6,7,8,9"
            });
                }
             
            }
            #region CRUD
            public GoodDao[] GetAllGoods()
            {
                return _goods.Where(x => !x.Deleted).ToArray();
            }

            public GoodDao GetGoodById(int? goodId)
            {
            GoodDao good = _goods.SingleOrDefault(x => x.Id == goodId);
                if (good != null && !good.Deleted)
                {
                    return good;
                }
                return null;
            }

            public bool AddNewGood(GoodDao good)
            {
                if (good.Id == 0)
                {
                    lock (ThisLock)
                    {
                    good.Id = ++_idCounter;
                    good.CreationDate = DateTime.Now;
                        _goods.Add(good);
                    }
                    return true;
                }
                return false;
            }

            public bool UpdateGood(GoodDao good)
            {
            GoodDao oldGood = GetGoodById(good.Id);
                if (oldGood != null && !oldGood.Deleted)
                {
                oldGood.Annotation = good.Annotation;
                oldGood.Text = good.Text;
                oldGood.Inner = good.Inner;
                oldGood.EditDate = DateTime.Now;
                    oldGood.TagsStr = good.TagsStr;
                return true;
                }
                return false;
            }

            public bool RemoveGoodById(int goodId)
            {
                lock (ThisLock)
                {
                    GoodDao good = GetGoodById(goodId);
                    if (good != null && !good.Deleted)
                    {
                        good.Deleted = true;
                        return true;
                    }
                }
                return false;
            }
            #endregion
        }
    }


          
