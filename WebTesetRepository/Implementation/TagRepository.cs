﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTesetRepository.API;
using WebTesetRepository.DaoModels;

namespace WebTesetRepository.Implementation
{
    public sealed class TagRepository : ITagRepository
    {

        private static List<TagDao> _tags;
        private static int _idCounter;
        private static readonly object ThisLock = new object();

        static TagRepository()
        {
            Random rand = new Random();
            _tags = new List<TagDao>();
            for (int i = 0; i < 100; i++)
            {
                _tags.Add(new TagDao
                {
                    Id = ++_idCounter,
                    Name = "Tag" + _idCounter,
                  });
            }

        }

        #region CRUD

        public IEnumerable<TagDao> GetTagByKey(string key)
        {
            if (key != null) { 
            IEnumerable<TagDao> tag = _tags.Where(x=>x.Name.ToLowerInvariant().Contains(key.ToLowerInvariant())).ToList();
            if (tag != null)
            {
                return tag;
            }
            }
            return null;
        }

        public bool AddNewTag(string tag)
        {
            string[] tags = tag.Split(',');
            foreach (var name in tags)
            {

                if (_tags.SingleOrDefault(x => x.Name == name) == null)
                {
                   lock (ThisLock)
                        {
                        TagDao tagMod = new TagDao();
                        tagMod.Id = ++_idCounter;
                            tagMod.Name = name;
                            _tags.Add(tagMod);
                        }
                        return true;
                    }
                }
            
            return true;
        }

        #endregion
    }
}