﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTesetRepository.DaoModels
{
   public class TagDao
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
