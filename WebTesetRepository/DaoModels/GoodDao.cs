﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTesetRepository.DaoModels
{
    public class GoodDao
    {
        public int? Id { get; set; }

      
        public string Inner { get; set; }

        public string Text { get; set; }
        public string Annotation { get; set; }
        public DateTime CreationDate { get; set; }

        
        public DateTime? EditDate { get; set; }

        public string TagsStr { get; set; }

        public bool Deleted { get; set; }
    }
}
