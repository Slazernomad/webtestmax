﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using Monitor.Controllers.Templates;
using WebTest.Controllers.ViewModels;
using WebTest.Services.API;
using WebTest.Services.DtoModels;

namespace WebTest.Controllers.ApiControllers
{
    public class GoodController : MainApiController
    {
        private readonly IGoodService _service;

        public GoodController(IGoodService service)
        {
            _service = service;
        }

        #region CRUD

        [HttpGet]
        public IEnumerable<GoodView> GetAllGoods()
        {
            IEnumerable<GoodDto> goods = _service.GetAllGoods();
            return Map<IEnumerable<GoodView>, IEnumerable<GoodDto>>(goods);
        }

        [HttpGet]
        public GoodView GetGoodById(int goodId)
        {
            GoodDto good = _service.GetGoodById(goodId);
            
           var b = Map<GoodView, GoodDto>(good);
            return Map<GoodView, GoodDto>(good);
        }

        [HttpPost]
        public HttpResponseMessage AddNewGood(GoodView good)
        {
            GoodDto newGood = Map<GoodDto, GoodView>(good);
            bool result = _service.AddNewGood(newGood);
            return HttpCode(result);
        }

        [HttpPut]
        public HttpResponseMessage UpdateGood(GoodView good)
        {
            GoodDto updGood = Map<GoodDto, GoodView>(good);
            bool result = _service.UpdateGood(updGood);
            return HttpCode(result);
        }

        [HttpDelete]
        public HttpResponseMessage RemoveGoodById(int goodId)
        {
            bool result = _service.RemoveGoodById(goodId);
            return HttpCode(result);
        }

        #endregion
    }
}
