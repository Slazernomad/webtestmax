﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Monitor.Controllers.Templates;
using WebTest.Controllers.ViewModels;
using WebTest.Services.API;
using WebTest.Services.DtoModels;

namespace WebTest.Controllers.ApiControllers
{
    public class TagController : MainApiController
    {
        private readonly ITagService _service;

        public TagController(ITagService service)
        {
            _service = service;
        }

        #region CRUD
  [HttpGet]
        public IEnumerable<TagView> GetTagByKey(string key)
        {
            IEnumerable<TagDto> tag = _service.GetTagByKey(key);
            return Map<IEnumerable<TagView>, IEnumerable<TagDto>>(tag);
        }

        [HttpGet]
        public HttpResponseMessage AddNewTag(string tag)
        {
         bool result = _service.AddNewTag(tag);
            return HttpCode(result);
        }

        #endregion
    }
}
