﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTest.Controllers.ViewModels
{
    public class GoodView
    {
        public int? Id { get; set; }
        public string Inner { get; set; }
        public string Text { get; set; }
        public string Annotation { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }
      public string TagsStr { get; set; }
    }
}