﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTest.Controllers.ViewModels
{
    public class TagView
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}