﻿using System.Linq;
using System.Web.Mvc;
using WebTest.Controllers.ViewModels;
using WebTest.Services.API;
using WebTest.Services.DtoModels;

namespace WebTest.Controllers.MVC
{
    public class HomeController : Controller
    {

        private readonly IGoodService _service;

        public HomeController(IGoodService service)
        {
            _service = service;
        }

        public ActionResult Goods()
        {
            return View();
        }

     
   
        [HttpGet]
        public ActionResult ShowGood(int goodId)
        {
            GoodDto good = _service.GetGoodById(goodId);
            GoodView goodView = new GoodView
            {
                EditDate = good.EditDate,
                Annotation = good.Annotation,CreationDate = good.CreationDate,Inner = good.Inner,Text = good.Text,Id = goodId,TagsStr = good.TagsStr
                };
        
            ViewBag.Good = goodView;
           
            return View();
        }

    }
}