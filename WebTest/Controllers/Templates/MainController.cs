﻿using System.Web.Mvc;
using ExpressMapper.Extensions;
using WebTesetRepository.API;

namespace Monitor.Controllers.Templates
{
    public abstract class MainController : Controller
    {
        public static T1 Map<T1, T2>(T2 source)
        {
            if (source != null)
            {
                return source.Map<T2, T1>();
            }
            return default(T1);
        }
    }
}
