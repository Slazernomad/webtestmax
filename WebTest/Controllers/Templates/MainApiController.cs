﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExpressMapper.Extensions;

namespace Monitor.Controllers.Templates
{
    public abstract class MainApiController : ApiController
    {
        //Maps DTO object to View object via AutoMapper
        public static T1 Map<T1, T2>(T2 source)
        {
            if (source != null)
            {
                return source.Map<T2, T1>();
            }
            return default(T1);
        }

        //Evaluates request procession result and forms HTTP status code accordingly.
        public HttpResponseMessage HttpCode(bool result)
        {
            if (result)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }
    }
}

