﻿using System.Web.Http;
using System.Web.Mvc;
using LightInject;
using WebTest.Services.API;
using WebTest.Services.Implementation;
using WebTesetRepository.API;
using WebTesetRepository.Implementation;

namespace WebTest
{
    public class LightInject
    {
        public static void Initialize()
        {
            var container = new ServiceContainer();

            container.RegisterControllers();
            container.RegisterApiControllers();
            //register other services

            container.EnablePerWebRequestScope();
            container.EnableWebApi(GlobalConfiguration.Configuration);

            // Service injection.
            //container.Register<IStoreService, StoreService>();
            container.Register<IGoodService, GoodService>();
            container.Register<ITagService, TagService>();

            // Repository injection.
            //container.Register<IStoreRepository, StoreRepository>();
            container.Register<IGoodRepository, GoodRepository>();
            container.Register<ITagRepository, TagRepository>();

            DependencyResolver.SetResolver(container);
        }
    }
}