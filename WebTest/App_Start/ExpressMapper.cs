﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExpressMapper;
using WebTesetRepository.DaoModels;
using WebTest.Controllers.ViewModels;
using WebTest.Services.DtoModels;

namespace WebTest
{
    public class ExpressMapper
    {
        public static void MappingRegistration()
        {
            Mapper.Register<GoodView, GoodDto>();
            Mapper.Register<GoodDto, GoodView>();
            Mapper.Register<GoodDto, GoodDao>();
            Mapper.Register<GoodDao, GoodDto>();

            Mapper.Register<TagView, TagDto>();
            Mapper.Register<TagDto, TagView>();
            Mapper.Register<TagDto, TagDao>();
            Mapper.Register<TagDao, TagDto>();



            Mapper.Compile();
        }
    }

   
}