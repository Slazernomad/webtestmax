﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebTesetRepository.API;
using WebTesetRepository.DaoModels;
using WebTest.Services.API;
using WebTest.Services.DtoModels;

namespace WebTest.Services.Implementation
{
    public sealed class TagService : GenericService, ITagService
    {
        private readonly ITagRepository _tagRepository;

        public TagService(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }

        public IEnumerable<TagDto> GetTagByKey(string key)
        {
            IEnumerable<TagDao> tag = _tagRepository.GetTagByKey(key);
            IEnumerable<TagDto> tagDto = Map<IEnumerable<TagDto>, IEnumerable<TagDao>>(tag);
            return tagDto;
        }

        public bool AddNewTag(string tag)
        {
            bool result = _tagRepository.AddNewTag(tag);
            return result;
        }
    }
    }
