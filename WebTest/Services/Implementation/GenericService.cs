﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExpressMapper.Extensions;

namespace WebTest.Services.Implementation
{
    public class GenericService
    {
        public static T1 Map<T1, T2>(T2 source)
        {
            if (source != null)
            {
                return source.Map<T2, T1>();
            }
            return default(T1);
        }
    }
}