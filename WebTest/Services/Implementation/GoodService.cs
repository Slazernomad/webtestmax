﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebTesetRepository.API;
using WebTesetRepository.DaoModels;
using WebTest.Services.API;
using WebTest.Services.DtoModels;

namespace WebTest.Services.Implementation
{
    public sealed class GoodService: GenericService, IGoodService
    {
      
            private readonly IGoodRepository _goodRepository;

        public GoodService(IGoodRepository goodRepository)
        {
            _goodRepository = goodRepository;
        }
        public IEnumerable<GoodDto> GetAllGoods()
            {
                IEnumerable<GoodDao> goods = _goodRepository.GetAllGoods();

                IEnumerable<GoodDto> goodsDto = Map<IEnumerable<GoodDto>, IEnumerable<GoodDao>>(goods);
                return goodsDto;
            }

            public GoodDto GetGoodById(int goodId)
            {

            GoodDao good = _goodRepository.GetGoodById(goodId);
           GoodDto goodDto = Map<GoodDto, GoodDao>(good);
                return goodDto;
            }

            public bool AddNewGood(GoodDto good)
            {
            GoodDao goodDao = Map<GoodDao, GoodDto>(good);
                bool result = _goodRepository.AddNewGood(goodDao);
                return result;
            }

            public bool UpdateGood(GoodDto good)
            {
            GoodDao goodDao = Map<GoodDao, GoodDto>(good);
                bool result = _goodRepository.UpdateGood(goodDao);
                return result;
            }

            public bool RemoveGoodById(int goodId)
            {
                bool result = _goodRepository.RemoveGoodById(goodId);
                return result;
            }
        }
    }

