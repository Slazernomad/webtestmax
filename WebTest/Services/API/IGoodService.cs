﻿using System.Collections.Generic;

using WebTest.Services.DtoModels;

namespace WebTest.Services.API
{
   public interface IGoodService
    {
        IEnumerable<GoodDto> GetAllGoods();
        GoodDto GetGoodById(int id);
        bool AddNewGood(GoodDto good);
        bool UpdateGood(GoodDto good);
        bool RemoveGoodById(int id);

    }
}
