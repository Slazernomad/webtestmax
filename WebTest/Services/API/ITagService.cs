﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTesetRepository.DaoModels;
using WebTest.Services.DtoModels;

namespace WebTest.Services.API
{
   public interface ITagService
    {
        IEnumerable<TagDto> GetTagByKey(string key);
        bool AddNewTag(string tag);
    }
}
