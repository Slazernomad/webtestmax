﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTest.Services.DtoModels
{
    public class TagDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}